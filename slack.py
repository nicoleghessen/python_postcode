import json
import requests
import sys

SLACK_URL = "https://hooks.slack.com/services/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"

########
API_URL = 'https://randomuser.me/api/'


api_response = json.loads(requests.get(API_URL).content)

# response = requests.get('https://randomuser.me/api/')
# parsed_content = json.loads(response.text)

########
message = f"A random surname is: { api_response ['results'][0]['name']['last'] }"

payload = f"""
{{
 "channel": "#academy_api_testing",
 "username": "Nicole",
 "text": "{message}",
 "icon_url": "https://i.kym-cdn.com/photos/images/newsfeed/001/813/481/5d7.jpg"
}}"""



r = requests.post(SLACK_URL, data=payload)

if r.status_code != 200:
    print(f"Something went wrong: status code {r.status_code}")
    exit()

print("Everything looks good")

