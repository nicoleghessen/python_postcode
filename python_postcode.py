import json
import requests


## what if we want 3 post codes?
## You can use a POST request 
### Post request have url + path + argument AND can take a JSON object
### (ALL request also have headers)

# this is a variable for the url
base_url = "http://api.postcodes.io/postcodes"
# arguments = ""

# data to be sent to api
json_post_codes = {
  "postcodes" : ["OX49 5NU", "M32 0JG", "NE30 1DP"]
}

# use the request library to make a post request
requests.post(url=base_url, data=json_post_codes)
print(requests.post(url=base_url, data=json_post_codes))

requests_post = requests.post(url=base_url, data=json_post_codes)

parsed_content =json.loads(requests_post.text)
print(parsed_content)
print(type(parsed_content))





# sending post request