import json
import requests
import sys

# file = open('randomuser.txt')
# content = file.read()
response = requests.get('https://randomuser.me/api/')

if response.status_code !=200:
    print("Something went wrong")
    sys.exit()
else:
    print("Got status 200 from the API")



print(f"I read the following information: {response.text}")

parsed_content =json.loads(response.text)
print("The parsed content is:")
print(parsed_content)

# print(f"deleted status of the third item: { parsed_content[3] ['deleted'] }")

print(f"A random surname is: { parsed_content ['results'][0]['name']['last'] }")
print(type(parsed_content))

