import json
import requests

# file = open('randomuser.txt')
# content = file.read()
content = requests.get('https://randomuser.me/api/')

print(f"I read the following information: {content}")

parsed_content =json.loads(content.text)
print("The parsed content is:")
print(parsed_content)

# print(f"deleted status of the third item: { parsed_content[3] ['deleted'] }")

print(f"deleted status of the third item: { parsed_content ['results'][0]['name']['last'] }")
print(type(parsed_content))

